# A Dependency Parser for Spoken Dialog Systems
This repository contains dependency parsed human-machine conversation (aka ConvBank) data described in
**A Dependency Parser for Spoken Dialog Systems** (Davidson et al., 2019) presented at EMNLP 2019.

We have also included our pretrained model for dependency parsing human-machine dialog.

Finally, we have included code to train your own models. This code is a modification of the dependency parser 
in [Dozat & Manning (2018)](https://arxiv.org/abs/1807.01396).
Code modified from source found [here](https://github.com/tdozat/Parser-v3).

We first train a model on the Universal Dependencies [English Web Treebank](https://universaldependencies.org/treebanks/en_ewt/index.html) (UD-EWT).

We then fine-tune this pretrained model using ConvBank data to create our final pretrained model for parsing of dialog data.

To parse data in enhanced UD CONLLU format using this pretrained model, run as follows:\
Ensure that all dependencies are installed: `pip install -r code/requirements.txt`\
Run the parser:\
`python main.py --save_dir model/mymodel_ewt_finetune/English-EWT/ParserNetwork run --output_dir OUTPUT_DIR INPUT_FILE.conllu`

Sentences should be in CONLLU format prior to parsing. To convert text to CONLLU format, 
you can use an online system such as [Aborator](https://corpling.uis.georgetown.edu/arborator/), 
or you can parse a text file using [StanfordCoreNLP dependency parser](https://nlp.stanford.edu/software/nndep.html).
Using the CoreNLP parser will result in a dependency parsed CONLLU file parsed using the CoreNLP pretrained model.
To reparse the file using our pretrained model for dialog systems, run the resulting CONLLU file through our system 
as described above.
